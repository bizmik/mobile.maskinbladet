<?php

$loader = require_once __DIR__ . '/../protected/vendor/autoload.php';
$loader->add('MBO', __DIR__ . '/MBO');

$app = new Silex\Application();
$app['config'] = include_once(__DIR__ . '/../protected/config.php');

$app['debug'] = $app['config']['debug']; // Debug mode

// Register Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../protected/views',
));

// Twig Customisations
$app['twig'] = $app->share($app->extend('twig', function ($twig, $app) {
    // Add globals for twig
    $twig->addGlobal('site_url', $app['config']['base_url']);
    $twig->addGlobal('config', $app['config']);

    $twig->addExtension(new \MBO\Extensions\Twig());
    return $twig;
}));

$app['api'] = $app->share(function ($app) {
    return new \MBO\Services\Api($app['config']['apiUrl']);
});


// ------------ Routes ----------------

// Front page
$app->get('/', function () use ($app) {
    $page = new \Controllers\Landing($app);
    return $app['twig']->render('landing.twig', $page->getData());
});


/*----------------  News  ------------------*/
// Latest news
$app->get('/news/list/latest', function () use ($app) {
    $articles = $app['api']->fetchLatestNews(30, 1);
    return $app['twig']->render('news-latest.twig', array('articles' => $articles));
});

// Popular news
$app->get('/news/list/popular', function () use ($app) {
    $articles = $app['api']->fetchPopularNews(30, 1);
    return $app['twig']->render('news-popular.twig', array('articles' => $articles));
});

// News detail page
$app->get('/news/view/{id}', function ($id) use ($app) {
    $article = $app['api']->fetchNewsArticle($id);
    return $app['twig']->render('news-detail.twig', array('article' => $article));
});

// Search news
$app->get('/news/list/search', function () use ($app) {
    $articles = $app['api']->searchNews();
    return $app['twig']->render('news-search.twig', array('articles' => $articles));
});

/*----------------  Events  ------------------*/
// Events list
$app->get('/event/list', function () use ($app) {
    $events = $app['api']->fetchEvents(30, 1);
    return $app['twig']->render('event-list.twig', array('events' => $events));
});

// Events detail page
$app->get('/event/view/{id}', function ($id) use ($app) {
    $event = $app['api']->fetchEventById($id);
    return $app['twig']->render('event-detail.twig'
        //,array('event' => $event)
    );
});

/*----------------  Machines  ------------------*/

/*----------------  Staff  ------------------*/
// Events list
$app->get('/staff/list', function () use ($app) {
    $staff = $app['api']->fetchStaff(30, 1);
    return $app['twig']->render('staff-list.twig', array('staff' => $staff));
});

/*----------------  Estates  ------------------*/

/*----------------  Dealers  ------------------*/


$app->run();
