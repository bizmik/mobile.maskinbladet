<?php

return array_merge(
    array(
        'apiUrl' => 'http://maskinbladet.dk/REST/',

        /** Layout and routers */
        'menu' => array(
            'Nyheder' => array(
                'news/list/latest' => 'Seneste',
                'news/list/popular' => 'Mest læste',
                'news/list/search' => 'Søg',
            ),
            'event/list' => 'Arrangementer',
            'staff/list' => 'Nyt om navne'
        ),
    ), include_once 'local.php');