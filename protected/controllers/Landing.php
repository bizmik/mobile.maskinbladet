<?php

namespace Controllers;

use Silex\Application;

class Landing
{
    private $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function getData()
    {
        $breaking = $this->getBreakingNews();
        $latest = $this->getLatestNews();
        $events = $this->getEvents();
        $staff = $this->getStaff();

        return array(
            'featured' => array_shift($breaking),
            'breakingGrid1' => array_slice($breaking, 0, 6),
            'breakingGrid2' => array_slice($breaking, 6, 6),
            'breakingList' => array_slice($breaking, 12, 5),
            'latestList1' => array_slice($latest, 0, 5),
            'latestList2' => array_slice($latest, 5, 5),
            'events' => $events,
            'staff' => $staff,
        );
    }

    public function getBreakingNews()
    {
        return $this->app['api']->fetchBreakingNews(18, 1);
    }

    public function getLatestNews()
    {
        return $this->app['api']->fetchLatestNews(10, 1);
    }

    public function getEvents()
    {
        return $this->app['api']->fetchEvents(5, 1);
    }

    public function getStaff()
    {
        return $this->app['api']->fetchStaff(5, 1);
    }
}