<?php

namespace MBO\Extensions;

class Twig extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('imageSize', array($this, 'imageSize')),
        );
    }

    public function imageSize($url, $size)
    {
        $url = preg_replace('#(\/imagecache)\/(\d+x\d+)#is', "$1/" . $size, $url);

        return $url;
    }

    public function getName()
    {
        return 'imageSize';
    }
}