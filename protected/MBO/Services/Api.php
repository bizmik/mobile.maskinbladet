<?php

namespace MBO\Services;

use Silex\Application;

class Api
{
    private $apiUrl;

    public function __construct($apiUrl)
    {
        $this->apiUrl = $apiUrl;
    }

    /** NEWS */

    /**
     * Returns latest news
     *
     * @param int $count number of records to return
     * @param int $offset page number
     * @param string $imageSize
     * @return object
     */
    public function fetchLatestNews($count = 10, $offset = 1, $imageSize = '')
    {
        return $this->callApi(
            'articles',
            array(
                'result_type' => 'normal',
                'count' => $count,
                'page' => $offset
            )
        );
    }

    /**
     * Returns breaking news
     *
     * @param int $count number of records to return
     * @param int $offset page number
     * @param string $imageSize
     * @return object
     */
    public function fetchBreakingNews($count = 10, $offset = 1, $imageSize = '')
    {
        return $this->callApi(
            'articles',
            array(
                'result_type' => 'breaking',
                'count' => $count,
                'page' => $offset
            )
        );
    }

    /**
     * Returns most read news
     *
     * @param int $count number of records to return
     * @param int $offset page number
     * @param string $imageSize
     * @return object
     */
    public function fetchPopularNews($count = 10, $offset = 1, $imageSize = '')
    {
        return $this->callApi(
            'articles',
            array(
                'result_type' => 'popular',
                'count' => $count,
                'page' => $offset
            )
        );
    }

    /**
     * Returns list of news base on a search query
     *
     * @param int $count number of records to return
     * @param int $offset page number
     * @param string $query
     * @param string $imageSize
     * @return object
     */
    public function searchNews($count = 10, $offset = 1, $query = '', $imageSize = '')
    {
        return $this->callApi(
            'articles',
            array(
                'q' => $query,
                'result_type' => 'search',
                'count' => $count,
                'page' => $offset
            )
        );
    }

    /**
     * Returns news article by its ID
     *
     * @param int $id
     * @param string $imageSize
     * @return object
     */
    public function fetchNewsArticle($id, $imageSize = '')
    {
        return $this->callApi(
            'articles',
            array(
                'id' => $id,
                'result_type' => 'normal',
                'related_type' => 'extended'
            )
        );
    }

    /** EVENTS */

    /**
     * Returns a list of events
     *
     * @param int $count number of records to show
     * @param int $offset page number
     * @return object
     */
    public function fetchEvents($count = 10, $offset = 1)
    {
        return $this->callApi(
            'events',
            array(
                'count' => $count,
                'page' => $offset
            )
        );
    }

    /**
     * Returns event by id
     *
     * @param $id
     * @return object
     */
    public function fetchEventById($id)
    {
        return $this->callApi(
            'events',
            array(
                'id' => $id,
                'result_type' => 'normal',
                'related_type' => 'extended'
            )
        );
    }

    /** STAFF NEWS */

    /**
     * Return list of staff news
     *
     * @param int $count number of records to show
     * @param int $offset page number
     * @return object
     */
    public function fetchStaff($count = 10, $offset = 1)
    {
        return $this->callApi(
            'names',
            array(
                'count' => $count,
                'page' => $offset
            )
        );
    }

    /** DEALERS */

    /**
     * Return a list of all available dealers
     *
     * @return object
     */
    public function fetchDealers()
    {
        return $this->callApi(
            'dealers',
            array()
        );
    }

    /** MACHINES */

    /**
     * Returns a list of featured machines
     *
     * @param int $count number of records to show
     * @param int $offset page number
     * @param string $imageSize
     * @return object
     */
    public function fetchFeaturedMachines($count = 10, $offset = 1, $imageSize = '')
    {
        return $this->callApi(
            'machines',
            array(
                'result_type' => 'featured',
                'count' => $count,
                'page' => $offset
            )
        );
    }

    /**
     * Returns a list of the most recently added machines
     *
     * @param int $count number of records to show
     * @param int $offset page number
     * @param string $imageSize
     * @return object
     */
    public function fetchLatestMachines($count = 10, $offset = 1, $imageSize = '')
    {
        return $this->callApi(
            'machines',
            array(
                'result_type' => 'latest',
                'count' => $count,
                'page' => $offset
            )
        );
    }

    /**
     * Gets machine details by its ID
     *
     * @param string $id
     * @param string $imageSize
     * @return object
     */
    public function fetchMachine($id, $imageSize = '')
    {
        return $this->callApi(
            'machines',
            array(
                'result_type' => 'details',
                'ad' => $id
            )
        );
    }

    /**
     * Return a complete multidimensional list of machine categories
     *
     * @return object
     */
    public function fetchMachineCategories()
    {
        return $this->callApi(
            'machines',
            array()
        );
    }

    /**
     * Performs and API call with given action and its parameters
     *
     * @param string $action
     * @param array $parameters
     * @return object
     */
    public function callApi($action, $parameters)
    {
        $url = $this->apiUrl . $action . '?' . http_build_query($parameters);

        if($response = @file_get_contents($url)) {
            return json_decode($response);
        }
        else {
            die('Api call error');
        }
    }
}